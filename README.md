[![build status](https://gitlab.com/sajay.surya/docker.hugo/badges/master/build.svg)](https://gitlab.com/sajay.surya/docker.hugo/commits/master)
---
* Contains the latest hugo release from https://github.com/gohugoio/hugo/releases
* Pushes monthly updates to Docker
