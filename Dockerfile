FROM fedora:latest

RUN dnf --assumeyes install git python3-pygments wget openssl
RUN wget "https://github.com$(wget -qO- https://github.com/gohugoio/hugo/releases/latest | grep Linux-64bit.tar.gz | sed '$d' | cut -d '"' -f 2 | sed -n 1p)"
RUN tar -xvzf *.tar.gz -C /bin/
RUN hugo version
